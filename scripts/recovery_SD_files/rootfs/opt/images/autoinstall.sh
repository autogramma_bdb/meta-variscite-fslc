#!/bin/bash
# Meant to be called by install_android.sh
# Added GPT feature for Android 7.1.2_r9

LOG_STDOUT="/opt/install_stdout.log"
LOG_STDERR="/opt/install_stderr.log"
#LOG_STDOUT="/dev/kmesg"
#LOG_STDERR="/dev/kmesg"

DEPLOY_LINK=/opt/images/Linux
#ISO_DEST_LINK=$2
DEV=$1
DEST_DEV=/dev/$DEV
DISK_SIZE=$(cat /sys/block/$DEV/size)


CFG_START=2186
CFG_SIZE=20480
KERNEL_SIZE=20480
EXTENDED_SIZE=1 # MBR extended partition
USERID_SIZE=20480
ROOT_SIZE=$(expr \( $DISK_SIZE - $CFG_START - $CFG_SIZE - $KERNEL_SIZE \* 2 - $USERID_SIZE - $EXTENDED_SIZE - 100 \) / 2)

CFG_END=$(expr $CFG_START + $CFG_SIZE)

KERNEL1_START=$(expr $CFG_END + 1)
KERNEL1_END=$(expr $KERNEL1_START + $KERNEL_SIZE)

KERNEL2_START=$(expr $KERNEL1_END + 1)
KERNEL2_END=$(expr $KERNEL2_START + $KERNEL_SIZE)

EXTENDED_START=$(expr $KERNEL2_END + 1)

ROOT1_START=$(expr $KERNEL2_END + 2)
ROOT1_END=$(expr $ROOT1_START + $ROOT_SIZE)

ROOT2_START=$(expr $ROOT1_END + 2)
ROOT2_END=$(expr $ROOT2_START + $ROOT_SIZE)

USERID_START=$(expr $ROOT2_END + 2)
USERID_END=$(expr $USERID_START + $USERID_SIZE)

CFG_DEV=${DEST_DEV}p1
KERNEL1_DEV=${DEST_DEV}p2
KERNEL2_DEV=${DEST_DEV}p3
# EXTENDED is p4
ROOT1_DEV=${DEST_DEV}p5
ROOT2_DEV=${DEST_DEV}p6
USERID_DEV=${DEST_DEV}p7

SPL_NAME_TGT=SPL-nand
UBOOT_NAME_TGT=u-boot.img-nand

UIMAGE_NAME=uImage
ROOTFS_NAME=fsl-image-gui-var-som-mx6.ext4
INSTALL_STEP=0

echo "================================================" >> $LOG_STDERR >> $LOG_STDOUT
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDERR >> $LOG_STDOUT
echo "================================================" >> $LOG_STDERR >> $LOG_STDOUT

function die
{
	echo "ОШИБКА: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации" | tee /dev/kmsg
	exit 1
}

function delete_device
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Удаление текущих разделов"

	dd if=/dev/zero of=${DEST_DEV}   bs=1M count=2      >> $LOG_STDOUT 2>> $LOG_STDERR || true
	sync; sleep 1
}

function create_parts
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Содание новых разделов"

	umount ${DEST_DEV}p*
	parted --script ${DEST_DEV} \
		mklabel msdos \
		mkpart primary ${CFG_START}s ${CFG_END}s \
		mkpart primary ${KERNEL1_START}s ${KERNEL1_END}s \
		mkpart primary ${KERNEL2_START}s ${KERNEL2_END}s \
		mkpart extended ${EXTENDED_START}s 100% \
		mkpart logical ${ROOT1_START}s ${ROOT1_END}s \
		mkpart logical ${ROOT2_START}s ${ROOT2_END}s \
		mkpart logical ${USERID_START}s ${USERID_END}s \
		print >> $LOG_STDOUT 2>> $LOG_STDERR || die

	if [ $? -ne 0 ]; then die; fi

	partx -u ${DEST_DEV} || die
}

function install_bootloader
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика"

	echo " ... : Очистка NAND"
	flash_erase /dev/mtd0 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	flash_erase /dev/mtd1 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die

	echo " ... : Установка NAND загрузчика"

	kobs-ng init -x ${DEPLOY_LINK}/${SPL_NAME_TGT} --search_exponent=1 -v >> $LOG_STDOUT 2>> $LOG_STDERR || die
	nandwrite -p /dev/mtd1 ${DEPLOY_LINK}/${UBOOT_NAME_TGT} >> $LOG_STDOUT 2>> $LOG_STDERR || die
}

function install_linux
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика и ядра Linux"

	echo " ... : Установка раздела ядра: $bootimage_file"
	umount ${DEST_DEV}p*
	mkdir /mnt/BOOT-VARMX6

	mkfs.ext4 ${KERNEL1_DEV} -FFL kernel1 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	mount ${KERNEL1_DEV} /mnt/BOOT-VARMX6 >> $LOG_STDOUT 2>> $LOG_STDERR || die

	cp ${DEPLOY_LINK}/*.dtb          "/mnt/BOOT-VARMX6/"             >> $LOG_STDOUT 2>> $LOG_STDERR || die
	cp "${DEPLOY_LINK}/${UIMAGE_NAME}" "/mnt/BOOT-VARMX6/$UIMAGE_NAME" >> $LOG_STDOUT 2>> $LOG_STDERR || die

	sync
	sleep 1
	umount -dl ${KERNEL1_DEV} >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	rm -rf /mnt/BOOT-VARMX6
	sync

	echo " ... : Установка SPL, u-boot на EMMc:"
	dd if=${DEPLOY_LINK}/${SPL_NAME_TGT} of=${DEST_DEV} bs=512 seek=2 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	dd if=${DEPLOY_LINK}/${UBOOT_NAME_TGT} of=${DEST_DEV} bs=512 seek=138 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	echo " ... : Установка образа rootfs: ${DEPLOY_LINK}/${ROOTFS_NAME}"
	dd if=${DEPLOY_LINK}/${ROOTFS_NAME} of=${ROOT1_DEV} >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	e2label ${DEST_DEV}p5 root1 >> $LOG_STDOUT 2>> $LOG_STDERR || die

	echo " ... : Установка точек монтирования раздела root1"
	mkdir /mnt/root1
	mount ${DEST_DEV}p5 /mnt/root1

	echo " ... : Расширение раздела root1"
	resize2fs ${DEST_DEV}p5 >> $LOG_STDOUT 2>>$LOG_STDERR || die

	mkdir /mnt/tmp
	cp -r /mnt/root1/etc/bdb-default-cfg/* /mnt/tmp

	mkdir /mnt/root1/etc/git_os_info
	cp -r /etc/git_os_info/* /mnt/root1/etc/git_os_info

	umount -dl /mnt/root1
	rm -rf /mnt/root1
	sync

	sleep 1
}

function check_fs
{
	label=$1
	dev=$2
	echo " ... : Проверка файловой системы $label на $dev"
	fsck.ext4 -n "$dev" >> $LOG_STDOUT 2>> $LOG_STDERR
	if [ $? -eq 0 ]; then
		echo " ... : $label на $dev существует и исправна, оставляем без изменений"
	else
		echo " ... : $label на $dev ИМЕЕТ СБОЙ с кодом $? ПЕРЕФОРМАТИРУЕМ!"
		mkfs.ext4 -FFL "$label" "$dev" >> $LOG_STDOUT 2>> $LOG_STDERR || die
	fi
}

function setup_other_fs
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Проверка/Раворачивание пользовательских файловых сиситем."

	check_fs "CFG" "${CFG_DEV}"

	sync; sleep 1

	check_fs "userid" "${USERID_DEV}"

	echo " ... : Установка стандартных файлов конфигураций на userid"
	mkdir /mnt/userid
	mount ${USERID_DEV} /mnt/userid -t ext4 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	cp -vr /mnt/tmp/* /mnt/userid
	rm -rf /mnt/tmp
	sync
	umount -dl /mnt/userid >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync
	rm -rf /mnt/userid

# Неиспользуется на BDB
#	echo " ... : Копируем ключи для доступа к ssh-tunnel на userid"
#	mkdir -p /mnt/userid/ssh-tunnel/.ssh
#	cp -r /opt/images/ssh-tunnel/.ssh/* /mnt/userid/ssh-tunnel/.ssh

# Неиспользуется на BDB
#	sleep 1
#	echo " ... : Создание файловой системы userdt: на ${DEST_DEV}p8"
#	mkfs.ext4 -FFL userdt ${DEST_DEV}p8 >> $LOG_STDOUT 2>> $LOG_STDERR || die
#	sync

	echo " "| tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " Внимание работа установщика завершена! Питание можно отключить!" | tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " "| tee /dev/kmsg


	source /etc/git_os_info/git_hash

	echo " Установлен софт:" | tee /dev/kmsg
	echo "kernel              git hash $rev_kernel" | tee /dev/kmsg
	echo "uboot               git hash $rev_uboot" | tee /dev/kmsg
	echo "meta-variscite-fslc git hash $meta_variscite_fslc" | tee /dev/kmsg
	echo "agl                 git hash $agl" | tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " "| tee /dev/kmsg

	# yaft должен успеть обновить экран на Solo перед тем как завершит работу
	sleep 3s
}

install_bootloader
delete_device
create_parts
install_linux
setup_other_fs

