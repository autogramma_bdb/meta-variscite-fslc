#!/bin/sh

if [ "$1" != "" ]; then
	SERTERM=$1
else
	SERTERM=`dmesg|grep "converter now attached to ttyUSB"|tail -n 1| sed -e 's/.*ttyUSB//g'`
fi

echo "set line /dev/ttyUSB$SERTERM
set speed 115200
set carrier-watch off
set flow-control none
set prefixing all
set parity none
set stop-bits 1
set modem none
set file type bin
set file name lit
connect" > /tmp/ttyUSB$SERTERM

kermit /tmp/ttyUSB$SERTERM
