SUMMARY = "MCM DisplayTest"
DESCRIPTION = "MCM DisplayTest application"
APP_VARIANT = "DisplayTestMCM"

# Включаю рецепт сборки AGL, поскольку displaytest тоже основан на AGL
# Но хостится в отдельном репозитории
include agl.inc
