SUMMARY = "Meta package for building an installable SDK for Autogramma OS"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit populate_sdk

# Add SDL2
TOOLCHAIN_HOST_TASK_append = " nativesdk-libsdl2 "
TOOLCHAIN_TARGET_TASK_append = " \
	libsdl2 \
	libsdl2-dev \
	libpng-dev \
	imx-vpu \
	imx-gpu-g2d \
	libgles2-imx \
	zlib-dev \
	glew \
	glew-dev \
	glm-dev \
	libstdc++-staticdev \
	gcc-sanitizers \
 "
