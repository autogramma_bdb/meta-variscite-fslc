SUMMARY = "BDB Dashboard"
AUTHOR = "Autogramma"
DESCRIPTION = "BDB Dashboard application in Qt Quick"
HOMEPAGE = "https://www.autogramma.ru"

inherit qmake5 systemd

LICENSE = "CLOSED"
RDEPENDS_${PN} = " qtbase qtquickcontrols2 qtquickcontrols qtserialport qtmultimedia qtserialbus qtgraphicaleffects bash "
DEPENDS += "${RDEPENDS_${PN}} qtdeclarative-native "
SRC_URI = "file://./"

# PACKAGECONFIG ?= "qtquickcompiler"

S = "${WORKDIR}"

do_install_append() {
  install -d ${D}/opt/dashboard/bin
  install -m 0755 ${WORKDIR}/dashboard-update.sh      ${D}/opt/dashboard/bin

  install -d ${D}${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/dashboard.service        ${D}${systemd_unitdir}/system/
  install -m 0644 ${WORKDIR}/dashboard-update.service ${D}${systemd_unitdir}/system/

  mkdir -p ${D}${sysconfdir}/systemd/system/local-fs.target.wants/

  ln -sf ${systemd_unitdir}/system/dashboard.service \
     ${D}${sysconfdir}/systemd/system/local-fs.target.wants/dashboard.service
}

FILES_${PN} += "/opt/dashboard/bin ${systemd_unitdir}/system/ ${sysconfdir}/systemd/system/local-fs.target.wants/dashboard.service"

# временный хак, чтобы исправить ошибку
# "File /opt/dashboard/bin/dashboard from dashboard was already stripped, this will prevent future debugging! [already-stripped]"
INSANE_SKIP_${PN}_append = "already-stripped"

