# asc особый таргет, нет смысла объединять код с agl.inc
# include agl.inc

SUMMARY = "AGL Asset Compiler"
DESCRIPTION = "Asset Compiler for Autogramma projects"
AUTHOR = "Autogramma"
HOMEPAGE = "https://www.autogramma.ru"
LICENSE = "CLOSED"
DEPENDS = "libpng zlib"
SRC_URI = "file://agl"
S = "${WORKDIR}/agl"

inherit cmake pkgconfig native
BBCLASSEXTEND = "native"

EXTRA_OECMAKE = "\
  -DUSE_GLFW=OFF \
"

do_install() {
	install -d ${D}/${bindir}
	install -m 755 ${B}/asc/asc ${D}/${bindir}/asc
}

