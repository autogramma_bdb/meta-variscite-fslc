SUMMARY = "BDB Dashboard"
AUTHOR = "Autogramma"
DESCRIPTION = "BDB Dashboard application"
HOMEPAGE = "https://www.autogramma.ru"
LICENSE = "CLOSED"
DEPENDS = "libsdl2 libpng asc-native imx-vpu imx-gpu-g2d"
RDEPENDS_${PN} = "libsdl2 libpng imx-vpu imx-gpu-g2d"
RPROVIDES_${PN} = "agl"
SRC_URI = "file://./"
S = "${WORKDIR}"

inherit cmake systemd pkgconfig

# Генерация дат с Yocto не работает, так как процесс сборки и создания метаданных выходит недетерменированным
# BUILD_DATE := "${@time.strftime('%Y-%m-0%dT%H-%M-%S-stock', time.localtime())}"
BUILD_DATE = "0-stock"

EXTRA_OECMAKE = "\
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DCMAKE_INSTALL_PREFIX:PATH=/opt/app/${BUILD_DATE}/ \
  -DUSE_SDL=ON \
  -DAGL_TARGET_DEVICE=ON \
  -DOUTPUT_MAP=ON \
  -DAPP_VARIANT=${APP_VARIANT} \
  -DASC_BIN_PATH=${RECIPE_SYSROOT_NATIVE}/usr/bin/asc \
  -DUSE_VIVANTE_GAL=ON -DUSE_VIVANTE_PBUFFER=ON -DUSE_VPU=ON \
"

do_install_append() {
  mkdir -p ${D}/opt/app/config

  if [ ! -z "${APP_SUBVARIANT}" ]; then
    echo "${APP_SUBVARIANT}" > ${D}/opt/app/config/APP_SUBVARIANT
  fi

  cp -vr ${D}/opt/app/${BUILD_DATE} ${D}/opt/fallback
  ln -sf /opt/app/${BUILD_DATE} ${D}/opt/app/current

  install -d ${D}${systemd_unitdir}/system
  install -m 0644 ${S}/app.service        ${D}${systemd_unitdir}/system/
  install -m 0644 ${S}/app-fallback.service        ${D}${systemd_unitdir}/system/

  mkdir -p ${D}${sysconfdir}/systemd/system/local-fs.target.wants/

  ln -sf ${systemd_unitdir}/system/app.service \
     ${D}${sysconfdir}/systemd/system/local-fs.target.wants/app.service
}

FILES_${PN} += "\
  /opt/app/config \
  /opt/app/${BUILD_DATE} \
  /opt/app/current \
  /opt/fallback \
  ${systemd_unitdir}/system/ \
  ${sysconfdir}/systemd/system/local-fs.target.wants \
"

# временный хак, чтобы исправить ошибку
# "File /opt/dashboard/bin/dashboard from dashboard was already stripped, this will prevent future debugging! [already-stripped]"
# INSANE_SKIP_${PN}_append = "already-stripped"
