SUMMARY = "attach a serial line to an input-layer device"
AUTHOR = "Vojtech Pavlik"
DESCRIPTION = "Input line discipline attach program"
HOMEPAGE = "https://sourceforge.net/projects/linuxconsole/"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM="file://COPYING.GPLv2;md5=751419260aa954499f7abaabaa882bbe"
RDEPENDS_${PN} = ""
DEPENDS += ""
SRC_URI = "file://COPYING.GPLv2 file://serio-ids.h file://inputattach.c file://inputattach.service"
FILESEXTRAPATHS_prepend = "${THISDIR}/${PN}:"
S = "${WORKDIR}"

inherit systemd

do_compile() {
	${CC} ${CFLAGS} -c inputattach.c -o inputattach.o
	${CC} ${LDFLAGS} inputattach.o -o inputattach
}

do_install() {
	install -d ${D}/usr/bin/
	install -m 0755 ${S}/inputattach ${D}/usr/bin/inputattach

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/inputattach.service        ${D}${systemd_unitdir}/system/

	install -d ${D}${sysconfdir}/systemd/system/local-fs.target.wants/
	ln -sf ${systemd_unitdir}/system/inputattach.service ${D}${sysconfdir}/systemd/system/local-fs.target.wants/inputattach.service
}

FILES_${PN} = "${sysconfdir}/systemd/system/local-fs.target.wants/inputattach.service ${systemd_unitdir}/system/inputattach.service /usr/bin/inputattach"
