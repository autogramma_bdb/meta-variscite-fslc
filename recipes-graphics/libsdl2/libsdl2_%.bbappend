# EXTRA_OECONF_append = " --enable-video-kmsdrm "

PACKAGECONFIG_append_var-som-mx6 = " gles2 "
EXTRA_OECONF_remove = " --disable-video-vivante"
EXTRA_OECONF_append = " \
	--enable-video-vivante \
	--disable-render \
	--disable-power \
	--disable-filesystem \
	--disable-loadso \
	--disable-cpuinfo \
	--disable-file \
	--disable-haptic \
	--disable-joystick \
	--disable-audio \
	--disable-video-vulkan \
"
