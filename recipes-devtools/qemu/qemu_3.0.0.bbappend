FILESEXTRAPATHS_prepend := "${FILE_DIRNAME}/${PN}:"

SRC_URI_append_class-native = "file://syscall.c-add_missing_sockios_header_include.patch \
           file://0001-linux-user-assume-__NR_gettid-always-exists.patch \
           file://0001-linux-user-rename-gettid-to-sys_gettid-to-avoid-clas.patch \
           file://syscall.c-fix-stime.patch"
