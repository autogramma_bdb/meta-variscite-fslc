#!/bin/bash

#################################################################
#                                                               #
# Скрипт позволяет автоматизировать часто повторяющиеся задачи. #
# Все сценарии в данном файле рассчитаны на сборку Yocto для    #
# устройства МК монитора.                                       #
#                                                               #
#################################################################

# _IMAGE=fsl-image-gui
_MACHINE=var-som-mx6
_DISTRO=fslc-framebuffer-ag
_BUILD_DIR=build_fb
_DEFAULT_IMAGE=ag-bdb-image-trolleybus

RETURNDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DWHITE="\Z7"
DDEF="\Zn"

# Установка EDITOR
if [ "$EDITOR" = "" ]; then EDITOR=nano; fi

# Проверяем есть ли у нас программа dialog и конфиг
die() {
	echo "$1"
	exit 1
}

which dialog > /dev/null || die "Установите программу dialog перед запуском меню: sudo apt install dialog"

if [ ! -f menu.config.sh ]; then
	{
		echo "_IMAGE=$_DEFAULT_IMAGE" 
		for i in $(ls -1 sources/meta-variscite-fslc/recipes-autogramma/images/ | sed 's/.bb//g'); do
			echo "#_IMAGE=$i"
		done
	} > menu.config.sh

	dialog --extra-button --extra-label "Редактировать" --yesno "Инициализация конфигурации по умолчанию, продолжить?" 0 0
	
	if [ $? -eq 3 ]; then
		$EDITOR menu.config.sh
	fi
fi

source menu.config.sh

# Нужна также строчка машины с подчёркиваниями вместо дефисов
_MACHINE_=$(echo $_MACHINE|tr '-' '_')

items=() # Массив ключей и текстов для пунктов меню.
nodes=() # Массив блочных устройств и их размеров.

# Обновляет массив nodes.
nodes_array_update() {
	nodes=( $(lsblk | grep -E 'sd[a-z] ' | awk '{ print $1, "[" $4 "]" }') )
}

# Возвращает последнее блочное устройство, доступное в системе.
get_last_node() {
	nodes_array_update
	echo "${nodes[${#nodes[@]}-2]}"
}

# item_add <тег> <текст_пункта_меню>
# Добавляет новый элемент в в меню.
item_add() {
	local menu_index=$1
	local title=$2

	local index=$(( menu_index * 2 ))
	items[$index]=$1
	items[$index+1]="$title"
}

# item_title_by_index <тег_пункта_меню>
# Возвращает текст пункта меню по его тегу.
item_title_by_index() {
	echo "${items[$1*2+1]}"
}

# Отображает меню доступных сценариев.
# Возвращает тег выбранного пользователем  пункта меню.
show_menu() {
	# Количество пунктов меню в массиве.
	# В массиве записаны последовательно индекс элемент - текст - индекс...,
	#	поэтому размер массива делим пополам.
	local count=$(( ${#items[@]} / 2 ))

	local cols=0 # Автоматический подбор ширины
	local rows=$(( count + 7 ))
	
	local title=$1
	
	dialog --colors --title "$title" --default-item "$LAST_ITEM" --menu "Выберите один из пунктов:" $rows $cols $count "${items[@]}" 2>&1 > /dev/tty
}

# Инициализирует переменные для сборки
script_setup_build_env() {
	export MACHINE=$_MACHINE
	export DISTRO=$_DISTRO

	source ./setup-environment "$_BUILD_DIR"
}

# По идее пищит спикером, но на деле выводит уведомление от терминала
beep() {
	echo -e '\a'
}

script_build() {
	#bitbake -c cleanall core-image-minimal не уверен что работет как надо
	#bitbake -c cleansstate virtual/bootloader virtual/kernel эта штука работат
	#bitbake core-image-minimal
	#bitbake core-image-minimal -c deploy тестить надо
	#bitbake core-image-minimal -c compile -f тестить надо
	#bitbake core-image-minimal -c imx_v7_var_defconfig -f Собрать конкретный конфиг ядра

	#Без этого образы могуть собиратся надекватно.
	rm -rdf "${PWD}/out_iso"
	bitbake -c cleansstate kamaz-k5 trolleybus bus foreign-electrobus-london foreign-electrobus-argentina paz "$_IMAGE"
	time bitbake "$_IMAGE"
	cd "$RETURNDIR"
}

extract_hash_from_git_dir()
{
	local GIT_DIR=$1

	pushd "$GIT_DIR" > /dev/null
	git describe --always --dirty --long --abbrev=40
	popd > /dev/null
}

# Проверяем есть ли репозиторий в workspace
# Если да, берется хеш из него
# Если нет, берем из latest_srcrev
extract_srcrev_from_package_or_workspace()
{
	local PACKAGE=$1
	local GIT_DIR=$_BUILD_DIR/workspace/sources/$PACKAGE
	local LATEST_SRCREV_FILE=$_BUILD_DIR/buildhistory/packages/$_MACHINE_-fslc-linux-gnueabi/$PACKAGE/latest_srcrev
	local REV=

	if [ -d "$GIT_DIR" ]; then
		REV=$(extract_hash_from_git_dir "$GIT_DIR")
	else
		REV=$(grep -e "^SRCREV = " "$LATEST_SRCREV_FILE" | awk -F'"' '{print $2}')
	fi

	echo "$REV"
}

extract_rev_from_build_id()
{
	local BUILD_ID_FILE=$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/build-id.txt
	local KEY=$1

	REV=$(grep -e "^$KEY " "$BUILD_ID_FILE" | awk -F'"' '{ print $2 }' | sed 's/.*://g')

	echo "$REV"
}

script_out_extrainfo() {
	local DEST_INFO=$1

	rm -rf "$DEST_INFO"
	mkdir -p "$DEST_INFO"

	touch "$DEST_INFO/git_hash"
	touch "$DEST_INFO/list_files.txt"
	touch "$DEST_INFO/list_packages.txt"

	echo "# Environment file with Git hashes used to build this distribution"           | tee -a "$DEST_INFO/git_hash"

	echo "build_machine=$(whoami)@$(hostname)"                                          | tee -a "$DEST_INFO/git_hash"

	echo "rev_kernel=$(extract_srcrev_from_package_or_workspace linux-variscite)"       | tee -a "$DEST_INFO/git_hash"
	echo "rev_uboot=$(extract_srcrev_from_package_or_workspace u-boot-variscite)"       | tee -a "$DEST_INFO/git_hash"

	echo "meta_poky=$(extract_rev_from_build_id meta-poky)"                             | tee -a "$DEST_INFO/git_hash"
	echo "meta_multimedia=$(extract_rev_from_build_id meta-multimedia)"                 | tee -a "$DEST_INFO/git_hash"
	echo "meta_freescale=$(extract_rev_from_build_id meta-freescale)"                   | tee -a "$DEST_INFO/git_hash"
	echo "meta_freescale_3rdparty=$(extract_rev_from_build_id meta-freescale-3rdparty)" | tee -a "$DEST_INFO/git_hash"
	echo "meta_freescale_distro=$(extract_rev_from_build_id meta-freescale-distro)"     | tee -a "$DEST_INFO/git_hash"
	echo "meta_browser=$(extract_rev_from_build_id meta-browser)"                       | tee -a "$DEST_INFO/git_hash"
	echo "meta_python=$(extract_rev_from_build_id meta-python)"                         | tee -a "$DEST_INFO/git_hash"
	echo "meta_qt5=$(extract_rev_from_build_id meta-qt5)"                               | tee -a "$DEST_INFO/git_hash"
	echo "meta_swupdate=$(extract_rev_from_build_id meta-swupdate)"                     | tee -a "$DEST_INFO/git_hash"
	echo "meta_variscite_fslc=$(extract_rev_from_build_id meta-variscite-fslc)"         | tee -a "$DEST_INFO/git_hash"

	echo "agl=$(extract_hash_from_git_dir sources/meta-variscite-fslc/recipes-autogramma/agl/files)" | tee -a "$DEST_INFO/git_hash"
	echo "displaytest=$(extract_hash_from_git_dir sources/meta-variscite-fslc/recipes-autogramma/displaytest/files)" | tee -a "$DEST_INFO/git_hash"

	cp "./$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/files-in-image.txt" "$DEST_INFO/list_files.txt"
	cp "./$_BUILD_DIR/buildhistory/images/$_MACHINE_/glibc/$_IMAGE/installed-packages.txt" "$DEST_INFO/list_packages.txt"
}

script_out_additions() {
	local EXTRA_INFO=$1
	local DEST=$2
	cp -r "$EXTRA_INFO" "$DEST/info"
	cp ./scripts/other_tools/out_additions/* "$DEST"
}

menu_item_1_selected() {
	$EDITOR "$RETURNDIR/menu.config.sh"
	# На всякий случай лучше перезапуститься, мало ли что в будущем добавится в конфиг
	# source menu.config.sh
	die
}

menu_item_2_selected() {
	script_build
}

menu_item_3_selected() {
	bitbake -c cleansstate u-boot-variscite linux-variscite
	#костыля для косяка сборки извлеченного devtool-ом ядра.
	sync
	rm -rf "${PWD}/tmp/work-shared/$_MACHINE/kernel-source/"
	ln -s "${PWD}/workspace/sources/linux-variscite" "${PWD}/tmp/work-shared/$_MACHINE/kernel-source"
}

menu_item_4_selected() {
	cd "${RETURNDIR}" 
	mkdir -pv out_iso
	mkdir -pv extra_info
	script_out_extrainfo "$PWD/extra_info"
	beep

	# Определям папку для бекапа
	local arfolder=
	case $USER in
	omarov)
		arfolder=$HOME/sda1/bdb_iso_archive
		;;
	modelkin)
		# TODO
		;;
	esac

	sources/meta-variscite-fslc/yocto_iso_creator.sh "$_BUILD_DIR/tmp/deploy/images/$_MACHINE/" out_iso extra_info "$_IMAGE" "$_MACHINE" "$arfolder"
	script_out_additions "$PWD/extra_info" "$PWD/out_iso"
	rm -rf extra_info
}

menu_item_5_selected() {
	menu_item_3_selected
	menu_item_2_selected
	menu_item_4_selected
}

menu_item_6_selected() {
	repo sync -j1 -v
}

menu_item_7_selected() {

	if [[ $UID != "0" ]]; then
	    echo " "
	    echo "*******************************************************************************"
	    echo "Необходимы привелигии администратора для работы скрипта"
	    echo "*******************************************************************************"
	    echo " "
	fi
	
	sudo rm -f /etc/xinetd.d/tftp
	sudo touch /etc/xinetd.d/tftp
	sudo chmod -R 777 /etc/xinetd.d/tftp
	
	#это временное решение надо править стоковые env u-boot
	pushd "tmp/deploy/images/$_MACHINE/"
	ln -s -f "imx6q-ag-bdb-$_MACHINE.dtb"  imx6q-var-som-res.dtb
	ln -s -f "imx6dl-ag-bdb-$_MACHINE.dtb" imx6dl-var-som-res.dtb
	popd
	
	{
		echo "service tftp"
		echo "{"
		echo "     protocol = udp"
		echo "     port = 69"
		echo "     socket_type = dgram"
		echo "     wait = yes"
		echo "     user = nobody"
		echo "     server = /usr/sbin/in.tftpd"
		echo "     server_args = ${PWD}/tmp/deploy/images/$_MACHINE"
		echo "     disable = no"
		echo "}"
	} >> /etc/xinetd.d/tftp
	
	sudo service xinetd restart

	HOST_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
	# HOST_NET=`echo $HOST_IP | rev | cut -d. -f2- | rev`
	
	echo "*******************************************************************************"
	echo "Пакеты необходимы для работы сервера:"
	echo " sudo apt-get install xinetd tftpd tftp"
	echo " sudo service xinetd restart"
	echo " sudo apt-get install nfs-kernel-server"
	echo " "
	echo " Руками добаляем в файл /etc/exports:"
	echo " $PWD/tmp/deploy/images/nfs_root_mnt ${HOST_IP}0/24(fsid=0,no_subtree_check)"
	echo " $PWD/tmp/deploy/images/nfs_root_mnt/root_fs ${HOST_IP}0/24(rw,sync,no_subtree_check,crossmnt,no_root_squash)"
	echo " обновляем список экспортов: sudo exportfs -a"
	echo " "
	echo "Как грузится:"
	echo " 1) ловим загрузчик"
	echo " 2) прописываем IP хост машины в аргументы u-boot и СТАРТУЕМ!!! :"
	echo "  setenv serverip $HOST_IP"
	echo "  run netboot"
	echo " "
	echo "*******************************************************************************"
	
	mkdir -p tmp/deploy/images/nfs_root_mnt
	mkdir -p tmp/deploy/images/nfs_root_mnt/root_fs
	sudo chown nobody:nogroup tmp/deploy/images/nfs_root_mnt
	sudo chown nobody:nogroup tmp/deploy/images/nfs_root_mnt/root_fs
	
	sleep 1
	sync
	#хз почему но на 3-4 раз перестёт грузится с сервера это вроде помогает
	sudo service nfs-kernel-server restart
	sleep 1
    
	echo " "
	echo " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "
	echo " "
	echo " Чтобы избежать колапсов фаловых систем скрит блокирует процесс"
	echo " Сейчас root_fs примаунченный через NFS будет доступен в локальной подсети"
	echo " "
	echo "*******************************************************************************"
	echo " "
	sync
	sleep 3
	
	sudo mount -o loop "tmp/deploy/images/$_MACHINE/$_IMAGE-$_MACHINE.ext4" tmp/deploy/images/nfs_root_mnt/root_fs
	
	sync
	echo " Нажмите ENTER чтобы прекратить блокировку и отмаунтить root_fs "
	echo "              для ручного отмаунчивания юзай: "
	read -p " sudo umount -dl ${PWD}/tmp/deploy/images/nfs_root_mnt/root_fs "
	sleep 2
	sync
	sudo umount -dl tmp/deploy/images/nfs_root_mnt/root_fs
	sync
	cd ../
}

menu_item_q_selected() {
	cd ../
}

# Точка входа в сценарий.
main() {
	# Если переменные окружения не были настроены, то настраиваем.
	if [ -z "${DISTRO}" ]; then script_setup_build_env; fi

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM="0"; fi
	if [ -z "${LAST_NODE}" ]; then LAST_NODE="$(get_last_node)"; fi
	
	local title="Устройство: $DWHITE$_MACHINE$DDEF | Образ: $DWHITE$_IMAGE$DDEF"

	item_add 1 "Редактировать конфиг"
	item_add 2 "Сборка дистрибутива Yocto"
	item_add 3 "Очистка сборки Yocto(нужно при изменениях ядра/загрузчика)"
	item_add 4 "Сборка образа для запуска образа с SD"
	item_add 5 "Очистка, сборка Yocto и сборка SD образа"
	item_add 6 "Синхронизация дерева исходного кода(repo)"
	item_add 7 "Service: Инициализировать TFTP для сетевой загрузки ядра"
	item_add q "Выйти"
	LAST_ITEM=$(show_menu "$title")

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM='q'; fi
	clear
	menu_item_${LAST_ITEM}_selected
	beep # Показать уведомление, что мы завершили работу
}

main
