#@TYPE: Machine
#@NAME: Variscite DART-6UL
#@SOC: i.MX6UL
#@DESCRIPTION: Machine configuration for Variscite DART-6UL
#@MAINTAINER: Eran Matityahu <eran.m@variscite.com>
#
# http://www.variscite.com

MACHINEOVERRIDES =. "mx6:mx6ul:"
include conf/machine/include/imx-base.inc
include conf/machine/include/tune-cortexa7.inc
include conf/machine/include/variscite.inc

SDCARD_GENERATION_COMMAND_mx6ul = "generate_imx_sdcard"

SERIAL_CONSOLE = "115200 ttymxc0"

MACHINE_SOCARCH_FILTER_mx6ul = "libfslcodec libfslparser gst-fsl-plugins"

KERNEL_DEVICETREE = " \
		     imx6ull-var-dart-6ulcustomboard-emmc-sd-card.dtb \
		     imx6ull-var-dart-6ulcustomboard-emmc-wifi.dtb \
		     imx6ull-var-dart-6ulcustomboard-nand-sd-card.dtb \
		     imx6ull-var-dart-6ulcustomboard-nand-wifi.dtb \
		     imx6ull-var-som-concerto-board-emmc-sd-card.dtb \
		     imx6ull-var-som-concerto-board-emmc-wifi.dtb \
		     imx6ull-var-som-concerto-board-nand-sd-card.dtb \
		     imx6ull-var-som-concerto-board-nand-wifi.dtb \
		     imx6ul-var-dart-6ulcustomboard-emmc-sd-card.dtb \
		     imx6ul-var-dart-6ulcustomboard-emmc-wifi.dtb \
		     imx6ul-var-dart-6ulcustomboard-nand-sd-card.dtb \
		     imx6ul-var-dart-6ulcustomboard-nand-wifi.dtb \
		     imx6ul-var-som-concerto-board-emmc-sd-card.dtb \
		     imx6ul-var-som-concerto-board-emmc-wifi.dtb \
		     imx6ul-var-som-concerto-board-nand-sd-card.dtb \
		     imx6ul-var-som-concerto-board-nand-wifi.dtb \
		     imx6ulz-var-dart-6ulcustomboard-emmc-sd-card.dtb \
		     imx6ulz-var-dart-6ulcustomboard-emmc-wifi.dtb \
		     imx6ulz-var-dart-6ulcustomboard-nand-sd-card.dtb \
		     imx6ulz-var-dart-6ulcustomboard-nand-wifi.dtb \
		     imx6ulz-var-som-concerto-board-emmc-sd-card.dtb \
		     imx6ulz-var-som-concerto-board-emmc-wifi.dtb \
		     imx6ulz-var-som-concerto-board-nand-sd-card.dtb \
		     imx6ulz-var-som-concerto-board-nand-wifi.dtb \
		     "
KERNEL_IMAGETYPE = "zImage"

UBOOT_MAKE_TARGET = ""
SPL_BINARY = "SPL"
UBOOT_SUFFIX = "img"
UBOOT_CONFIG ??= "nand sd "
# The above order matters. The last one will be also called u-boot-${MACHINE} and will be used to create the sdcard.
UBOOT_CONFIG[sd] = "mx6ul_var_dart_mmc_defconfig,sdcard"
UBOOT_CONFIG[nand] = "mx6ul_var_dart_nand_defconfig,ubifs"

MACHINE_FIRMWARE_append = " linux-firmware-imx-sdma-imx6q"

MACHINE_EXTRA_RDEPENDS += " \
			   bcm43xx-utils \
			   linux-firmware-bcm4339 \
			   linux-firmware-bcm43430 \
			   brcm-patchram-plus \
			   "
