#@TYPE: Machine
#@NAME: Variscite VAR-SOM-MX6
#@SOC: i.MX6Q/D/DL/S
#@DESCRIPTION: Machine configuration for Variscite VAR-SOM-MX6 VAR-SOM-SOLO/DUAL DART-MX6
#@MAINTAINER: Eran Matityahu <eran.m@variscite.com>
#
# http://www.variscite.com

MACHINEOVERRIDES =. "mx6:mx6dl:mx6q:"
# doesn't work! >:(
#DEFAULTTUNE="cortexa9t2hf-neon"

include conf/machine/include/imx-base.inc
include conf/machine/include/tune-cortexa9.inc
include conf/machine/include/variscite.inc

SERIAL_CONSOLE = "115200 ttymxc0"

KERNEL_DEVICETREE = "imx6q-ag-bdb.dtb imx6dl-ag-bdb.dtb imx6q-ag-mcm.dtb imx6dl-ag-mcm.dtb"
KERNEL_IMAGETYPE = "uImage"

MACHINE_FEATURES_remove = "bluetooth touchscreen qemu-usermode rtc usbgadget"
# MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "0"

UBOOT_MAKE_TARGET = ""
SPL_BINARY          = "SPL"
UBOOT_SUFFIX        = "img"
UBOOT_CONFIG ??= "nand sd "
# The above order matters. The last one will be also called u-boot-${MACHINE} and will be used to create the sdcard.
UBOOT_CONFIG[sd] = "mx6var_som_sd_config,sdcard"
UBOOT_CONFIG[nand] = "mx6var_som_nand_config,ubifs"

# Set optimization flags here as DEFAULTTUNE doesn't add -mcpu=cortex-a9 for some reason
# TODO: test -ffast-math
TARGET_CFLAGS += " -O3 -mcpu=cortex-a9 "

MACHINE_EXTRA_RDEPENDS += " \
			   util-linux-sfdisk \
			   kbd \
			   kbd-consolefonts \
			   kbd-consoletrans \
			   kbd-keymaps \
			   kbd-unimaps \
			   mtd-utils \
			   e2fsprogs-mke2fs \
			   e2fsprogs-e2fsck \
			   e2fsprogs-tune2fs \
			   e2fsprogs-resize2fs \
			   nano \
			   socat \
			   can-utils iproute2 \
			   i2c-tools \
			   spitools \
			   util-linux \
			   dosfstools \
			   ethtool \
			   stm32flash \
                           inputattach \
			   "

MACHINE_EXTRA_RDEPENDS_remove = "\
	crda \
	bridge-utils \
	gptfdisk \
	hostapd \
	hdparm \
	iw \
	packagegroup-tools-bluetooth \
	tslib-calibrate \
	tslib-tests \
	u-boot-fw-utils \
	u-boot-splash \
	wpa-supplicant \
"
