# Copyright (C) 2015 Freescale Semiconductor
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Freescale Image to validate i.MX machines. \
This image contains everything used to test i.MX machines including GUI, \
demos and lots of applications. This creates a very large image, not \
suitable for production."
LICENSE = "MIT"

inherit core-image distro_features_check

### WARNING: This image is NOT suitable for production use and is intended
###          to provide a way for users to reproduce the image used during
###          the validation process of i.MX BSP releases.

IMAGE_FEATURES += " \
    ssh-server-openssh \
    hwcodecs \
    debug-tweaks \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', '', \
       bb.utils.contains('DISTRO_FEATURES',     'x11', 'x11-base x11-sato', \
                                                       '', d), d)} \
"

QT5_FONTS = " \
    ttf-dejavu-mathtexgyre \
    ttf-dejavu-sans \
    ttf-dejavu-sans-condensed \
    ttf-dejavu-sans-mono \
    ttf-dejavu-serif \
    ttf-dejavu-serif-condensed \
"

# a1ba: removed packagegroup-tools-bluetooth
CORE_IMAGE_EXTRA_INSTALL += " \
	packagegroup-imx-tools-audio \
	packagegroup-fsl-tools-gpu-external \
	${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'weston-init', '', d)} \
        ${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', 'weston-xwayland xterm', '', d)} \
        ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'xterm', '', d)} \
        ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'packagegroup-core-x11-sato-games', '', d)} \
	screen \
	minicom \
	openssl \
	openssh openssh-sftp-server openssh-sftp openssh-misc yaft \
        util-linux-partx \
	parted \
        libsdl2 \
        gcc-sanitizers \
        gdbserver gdb \
        strace \
	imx-vpu libimxvpuapi imx-gpu-g2d \
	ckermit rsync \
	icu libinput fontconfig ${QT5_FONTS} \
 "

# Due to the Qt samples the resulting image will not fit the default NAND size.
# Removing default ubi creation for this image
IMAGE_FSTYPES_remove = "multiubi wic.bmap wic.xz wic"

